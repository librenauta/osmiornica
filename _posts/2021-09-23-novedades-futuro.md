---
layout: post
title:  "Novedades futuro"
author: librenauta
categories: [ novedades ]
tiempo: pasado
inicio: 23/09/2021
tags: [ novedades ]
image: assets/images/novedad.png
background: /assets/images/background/fondo-1.png
---
## ¡Novedades!
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

Inscripción a: [osmiornicabiblioteca@gmail.com](mailto:osmiornicabiblioteca@gmail.com)
