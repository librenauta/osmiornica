---
layout: post
title:  "Evento futuro dos"
author: librenauta
categories: [ evento ]
tags: [ cultura libre, bigdata, privacidad, internet ]
image: assets/images/evento.png
background: /assets/images/background/fondo-1.png
featured: true
---
## ¡Evento!
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.

Inscripción a: [osmiornicabiblioteca@gmail.com](mailto:osmiornicabiblioteca@gmail.com)
Te enviamos el programa y si te anotás los primeros txts de la biblio ;)
