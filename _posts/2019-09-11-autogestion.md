---
layout: post
title:  "Autogestión"
author: Emilia
categories: [ general ]
tags: [ autogestion, fondos, aportes solidarios ]
image: assets/images/taller.png
background: /assets/images/background/fondo-1.png
end: 2020/11/10
donacion-1: https://mpago.la/1aXaLdf
donacion-2: https://mpago.la/1jsD9v1
donacion-3: https://mpago.la/2YBuTQS
donacion-4: https://mpago.la/2PMn79f
featured: true
---
## ¡Aportes!
Podés donar con microdonaciones y coso en los siguientes links

ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

{% include donacion-link.html %}
